<script src="{{URL::to('js/jquery.min.js')}}"></script>
<script src="{{URL::to('js/landio.min.js')}}"></script>
<script src="{{URL::to('js/bootstrap/alert.js')}}"></script>
<script src="{{URL::to('js/alertify/alertify.min.js')}}"></script>
<script src="{{URL::to('js/bootstrap-toggle.min.js')}}"></script>
<script src="{{URL::to('js/fakeLoader.min.js')}}"></script>
@if($_SERVER['REQUEST_URI'] == "/jeu")
    <script src="{{URL::to('game/apijs.js')}}"></script>
@endif

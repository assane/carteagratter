<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="A free HTML template and UI Kit built on Bootstrap" />
    <meta name="keywords" content="free html template, bootstrap, ui kit, sass" />
    <meta name="author" content="Peter Finlan and Taty Grassini Codrops" />
    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="img/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="img/favicon/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="img/favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="img/favicon/favicon-16x16.png" sizes="16x16">
    <link pummrel="manifest" href="img/favicon/manifest.json">
    <link rel="shortcut icon" href="img/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#663fb5">
    <meta name="msapplication-TileImage" content="img/favicon/mstile-144x144.png">
    <meta name="msapplication-config" content="img/favicon/browserconfig.xml">
    <meta name="theme-color" content="#663fb5">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ URL::to('css/landio.css') }}">
    <link rel="stylesheet" href="{{ URL::to('fontawesome/css/font-awesome.css') }}">

    <!-- Alertify Plugin -->
    <link rel="stylesheet" href="{{ URL::to('css/alertify/css/alertify.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('css/alertify/css/themes/default.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('css/alertify/css/themes/semantic.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('css/alertify/css/themes/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('css/styles.css') }}">
    <link rel="stylesheet" href="{{ URL::to('css/bootstrap-toggle.min.css') }}" >
    @yield('css')
</head>
<body class="bg-faded">

<!--NavBar -->
 @if($_SERVER['REQUEST_URI'] != "/jeu")
    @include('layouts.nav')
 @endif

<!-- Content Page-->
<div class="container container2">
    @yield('container')
</div>

<!-- Js --> 
@include('layouts.js');
@yield('js')
<script>

    $('#formavatar').submit(function(e) {
        e.preventDefault();
        var fd = new FormData(this);
        if($("#avatar_update").val()==""){
            alert("Veuiller remplir le champ nom");
            $("#avatar_update").focus();
            return false;
        }
        $.ajax({
            url: $(this).attr('action'),
            xhr: function() {
                var xhr = new XMLHttpRequest();
                return xhr;
            },
            type: 'post',
            processData: false,
            contentType: false,
            data: fd,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function(){
                $(".container2").html("<br /><p align='center'><i class='fa fa-refresh fa-spin fa-5x fa-fw' ></i><span class='sr-only'>Loading</span>'<p>");
            },
            success:function(data){
                if(data == 1){
                    alertify.alert('Success', 'Modification Effectuée !!!', function(){ alertify.success('Ok'); });
                    window.location.reload();
                }else{
                    alertify.alert('Error', "Erreur d'Enregistrement !!!", function(){ alertify.success('Ok'); });
                }
            },
            error:function () {
                alertify.alert('Error', "Erreur d'Enregistrement !!!", function(){ alertify.success('Ok'); });
            }
        });
    });
</script>
</body>
</html>

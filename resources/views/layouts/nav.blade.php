<nav class="navbar navbar-dark bg-inverse" >
    <div class="container">

        @if(Auth::check())
            <a class="navbar-brand" href="{{route('carte.index')}}"><h1>Maxi QUIZ</h1></a>
        @else
            <a class="navbar-brand" href="{{route('carte.login')}}"><h1>Maxi QUIZ</h1></a>
       @endif

        <a class="navbar-toggler hidden-md-up pull-xs-right" data-toggle="collapse" href="#collapsingNavbarInverse" aria-expanded="false" aria-controls="collapsingNavbarInverse">
            &#9776;
        </a>

        <a class="navbar-toggler navbar-toggler-custom hidden-md-up pull-xs-right" data-toggle="collapse" href="#collapsingMobileUserInverse" aria-expanded="false" aria-controls="collapsingMobileUserInverse">
            <span class="icon-user"></span>
        </a>

        <div id="collapsingNavbarInverse" class="collapse navbar-toggleable-custom" role="tabpanel" aria-labelledby="collapsingNavbarInverse">
            <ul class="nav navbar-nav pull-xs-right">
                @if(Auth::check())
                    <li class="nav-item nav-item-toggable">
                        <a class="nav-link" href="{{route('carte.getcreateuser')}}" >Ajouter User</a>
                    </li>
                    <li class="nav-item nav-item-toggable">
                        <a class="nav-link" href="{{route('carte.getlistusers')}}" >Listes Users</a>
                    </li>
                    <li class="nav-item nav-item-toggable">
                        <a class="nav-link" href="{{route('carte.index')}}">Ajouter Lot</a>
                    </li>
                    <li class="nav-item nav-item-toggable">
                        <a class="nav-link" href="{{route('carte.show')}}" >Listes Lots</a>
                    </li>
                    <li class="nav-item nav-item-toggable">
                        <a class="nav-link" href="/logout" >Deconnexion</a>
                    </li>
                @endif
            </ul>
        </div>

        <div id="collapsingMobileUserInverse" class="collapse navbar-toggleable-custom dropdown-menu-custom p-x-1 hidden-md-up" role="tabpanel" aria-labelledby="collapsingMobileUserInverse">
            <div class="media m-t-1">
                <div class="media-left">
                    <img src="img/face5.jpg" height="60" width="60" alt="Avatar" class="img-circle">
                </div>
                <div class="media-body media-middle">
                    <h5 class="media-heading">Joel Fisher</h5>
                    <h6>hey@joelfisher.com</h6>
                </div>
            </div>
            <a href="#" class="dropdown-item text-uppercase">View posts</a>
            <a href="#" class="dropdown-item text-uppercase">Manage groups</a>
            <a href="#" class="dropdown-item text-uppercase">Subscription &amp; billing</a>
            <a href="#" class="dropdown-item text-uppercase text-muted">Log out</a>
            <a href="#" class="btn-circle has-gradient pull-xs-right m-b-1">
                <span class="sr-only">Edit</span>
                <span class="icon-edit"></span>
            </a>
        </div>
    </div>
</nav>

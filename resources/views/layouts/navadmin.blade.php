<nav class="navbar navbar-dark bg-inverse" >
    <div class="container">

        <a class="navbar-brand" href="/">
            <span class="icon-logo"></span>
            <span class="sr-only">Land.io</span>
        </a>

        <a class="navbar-toggler hidden-md-up pull-xs-right" data-toggle="collapse" href="#collapsingNavbarInverse" aria-expanded="false" aria-controls="collapsingNavbarInverse">
            &#9776;
        </a>

        <a class="navbar-toggler navbar-toggler-custom hidden-md-up pull-xs-right" data-toggle="collapse" href="#collapsingMobileUserInverse" aria-expanded="false" aria-controls="collapsingMobileUserInverse">
            <span class="icon-user"></span>
        </a>

        <div id="collapsingNavbarInverse" class="collapse navbar-toggleable-custom" role="tabpanel" aria-labelledby="collapsingNavbarInverse">
            <ul class="nav navbar-nav pull-xs-right">

                <li class="nav-item nav-item-toggable">
                    <a class="nav-link" href="{{route('pharmacy.dashboard')}}">Tableau de Bord</a>
                </li>

                @if(Auth::user()->profile_id == 1)
                <li class="nav-item nav-item-toggable">
                    <a class="nav-link" href="{{route('utilisateurs.index')}}">Utilisateurs</a>
                </li>
                <li class="nav-item nav-item-toggable">
                    <a class="nav-link" href="{{route('pharmacies.index')}}">pharmacies</a>
                </li>
                @endif

                <li class="nav-item dropdown hidden-sm-down">
                    <a class="nav-link dropdown-toggle nav-dropdown-user" id="dropdownMenuInverse2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="{{ URL::to('avatar/'.Auth::user()->avatar) }}" height="40" width="40" alt="Avatar" class="img-circle" onclick="modal_avarar();"> <span class="icon-caret-down"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-user dropdown-menu-animated" aria-labelledby="dropdownMenuInverse2">
                        <div class="media">
                            <div class="media-left">
                                <img src="{{ URL::to('avatar/'.Auth::user()->avatar) }}" height="60" width="60" alt="Avatar" class="img-circle">
                            </div>
                            <div class="media-body media-middle">
                                <h5 class="media-heading">{{ Auth::user()->personne->prenom.' '.Auth::user()->personne->nom }}</h5>
                                <h6>{{Auth::user()->personne->email}}</h6>
                            </div>
                        </div>
                        <a class="dropdown-item text-uppercase">Dernière Connexion</a>
                        <a class="dropdown-item text-uppercase">{{ Auth::user()->last_login }}</a>
                        <a  class="dropdown-item text-uppercase">profile : {{Auth::user()->profile->libelle}}  </a>
                        <a  class="dropdown-item text-uppercase" data-toggle="modal" href='#modal-avatar'>Modifier avatar </a>
                        <a href="{{ route('pharmacy.logout') }}" class="dropdown-item text-uppercase text-muted">Déconnexion</a>
                    </div>
                </li>

            </ul>
        </div>

    </div>
</nav>

<div class="modal fade" id="modal-avatar">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formavatar" action="{{route('utilisateur.updateavatar')}}" method="post" enctype="multipart/form-data">
            <div class="modal-header">
                <h4 class="modal-title">Changement image de profile</h4>
            </div>
            <div class="modal-body">
                    <input type="file" name="avatar_update" id="avatar_update">
                    <input type="hidden" name="iduseravatar" id="iduseravatar" value="{{Auth::user()->id}}">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btnaupdateavatar">Enregistrer</button>
                <button type="reset" class="btn btn-default" data-dismiss="modal">Annuler</button>
            </div>
            </form>
        </div>
    </div>
</div>
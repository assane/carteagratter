@extends('layouts.master')

@section('title')
    Carte a gratter
@endsection

@section('container')

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <p align="center">  <h1>Ajouter un User</h1> </p>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li><p style="color: red;font-weight: bolder">{{ $error }}</p></li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    <strong style="color: green;font-weight: bolder">{{ Session::get('flash_message') }}</strong>
                </div>
            @endif

            <form id="formcreatelot" action="{{route('carte.storenewuser')}}" enctype="multipart/form-data" method="post">

                <fieldset class="form-group">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" />
                </fieldset>

                <fieldset class="form-group">
                    <input type="text" class="form-control" id="login" name="login" placeholder="email" />
                </fieldset>

                <fieldset class="form-group">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe"  />
                </fieldset>

                <fieldset class="form-group">
                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation"
                           placeholder="confirmer le mot de passe"  />
                </fieldset>

                <button type="submit" class="btn col-md-12 btn-primary">Ajouter</button>

            </form>

        </div>
    </div>

@endsection
@extends('layouts.master')

@section('container')

    <h2> Dashboard :</h2>

<div class="row">

    <div class="col-md-4">

    <div class="card card-inverse card-social text-xs-center">
        <div class="card-block has-gradient">
            <img src="{{ URL::to('avatar/'.Auth::user()->avatar) }}" height="90" width="90" alt="Avatar" class="img-circle">
            <h5 class="card-title"> {{Auth::user()->personne->prenom}} {{Auth::user()->personne->nom}} </h5>
            <h6 class="card-subtitle">{{Auth::user()->personne->email}}</h6>
        </div>
    </div>
    </div>

    <div class="col-md-8">
        <div class="card card-chart">
            <ul class="list-group">
                <li class="list-group-item complete">
                    <span class="label pull-xs-right">{!!   \App\Pharmacie::all()->count() !!}</span>
                    <span class="icon-status status-completed"></span> Total des Pharmacies
                </li>

                <li class="list-group-item">
                    <span class="label pull-xs-right">{!!   \App\Utilisateur::where('profile_id',2)->count() !!}</span>
                    <span class="icon-status status-backlog"></span> Total des Pharmaciens
                </li>

                <li class="list-group-item">
                    <span class="label pull-xs-right">{!!   \App\Utilisateur::where('profile_id',1)->count() !!}</span>
                    <span class="icon-status status-backlog"></span> Total des Administrateurs
                </li>
                <?php
                    $tab = \App\Pharmacie::where('etatpharmacie_id',1)->get();
                    $liste_pharmacie = "";
                    foreach ($tab as $t) {
                       $liste_pharmacie .= $t->nom."  |  ";
                    }
                ?>
                <li class="list-group-item">
                   <span class="icon-status status-noticket"></span> Pharmacie de garde  &nbsp; <label style="!important;font-weight: bolder;font-size: x-large;color: #ff003f" > {!! \App\Pharmacie::where('etatpharmacie_id',1)->count() !!}</label>
                   <marquee>{!!  $liste_pharmacie !!}</marquee>
                </li>
            </ul>
        </div>
    </div>
</div>



@endsection
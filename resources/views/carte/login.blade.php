@extends('layouts.master')

@section('container')

    <br>

   <form action="{{route('carte.connecter')}}" enctype="multipart/form-data" method="post" id="formlogin">

    <div class="card card-inverse card-social text-xs-center">

        <div class="card-block has-gradient">
            <img src="../img/useri_logo.png" height="90" width="90" alt="Avatar" class="img-circle">
            <h5  class="card-title textwait">Authentification</h5>
        </div>

        <div class="card-block">
            <div class="row">
                <div class="col-md-12 card-stat">
                    <div class="form-group has-icon-left form-control-name">
                        <label class="sr-only" for="login">Your name</label>
                        <input type="text" required class="form-control" id="login" name="login" placeholder="Login">
                    </div>
                    <div class="form-group has-icon-left form-control-password">
                        <label class="sr-only" for="password">Enter a password</label>
                        <input type="password" required class="form-control " id="password" name="password" placeholder="Enter a password" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

    </div>
       {{ csrf_field() }}
        <button class="btn btn-primary btn-block btn-lg has-gradient login">Connexion</button>
    </form>

@endsection

@section('js')
    <script>
        $('#formlogin').submit(function(e) {
            e.preventDefault();
            var fd = new FormData(this);
            $.ajax({
                url: $(this).attr('action'),
                xhr: function() {
                    var xhr = new XMLHttpRequest();
                    return xhr;
                },
                type: 'post', 
                processData: false,
                contentType: false,
                data: fd,
                beforeSend :function() {
                    $(".textwait").html("<i class='fa fa-circle-o-notch fa-spin fa-3x fa-fw'></i><span class='sr-only'>Loading...</span>'");
                },
                success: function(data) {
                    if(data == 1){
                        $(".textwait").html(" <label class=''> Success  </label> ");
//                        window.location.href = "{{route('carte.index')}}";
                          window.location.href = "{{route('carte.game')}}";
                    }else{
                        $(".textwait").html(" <label class=''>Erreur d'authentification veuillez reesayer</label> ");
                    }
                }
            });
        });

    </script>
@endsection
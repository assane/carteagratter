@extends('layouts.master')

@section('css')
    <link rel="stylesheet" href="{{ URL::to('game/apistyle.css') }}" >
    <style>
    body {
         /*background: url(avatar/web_bg.jpg) no-repeat center fixed;*/
         /*background: url(avatar/img_c_bg0.jpg) no-repeat center fixed;*/
        /* background: url(avatar/img_c_bg1.jpg) no-repeat center fixed;*/
         background: url(new/bg.png) no-repeat center fixed;
         /*background: url(avatar/img_c_bg0.jpg) no-repeat center fixed;*/
         -webkit-background-size: contain;
         background-size: contain;
    	}
    </style>
@endsection

@section('title')
    Game
@endsection

@section('container')

    <input type='hidden' name='ref' id='ref' value='{{$lot->reference}}'>
    <input type='hidden' name='userid' id='userid' value='{{Auth::user()->id}}'>
    <input type='hidden' name='nombre' id='nombre' value='{{$lot->nombre}}'>

@if($lot->nombre == 0 && $lot->reference != 'M_win_loser')
    <img class='empty img-responsive' src='avatar/empty.png' />
@else
     <div class='container3' id='js-container'>
        <canvas class='canvas' id='js-canvas' width='689' height='375' ></canvas>
        <img class='form' style='visibility: hidden;' src='avatar/{{$lot->image}}'  />
    </div>
@endif

@endsection

@section('js')

<script>

function updateLot (){
           var ref =   $('#ref').val();
           var userid =   $('#userid').val();
           var nombre =   $('#nombre').val();
              $.ajax({
                url: "http://127.0.0.1:1001/api/updatelot",
                type: "POST",
                async : true,
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data  : {
                    'ref':ref,
                    'userid':userid,
                    'nombre':nombre
                 },
                success:function(s){
                  console.log('Status === '+s.statusCode+' '+s.message);
                },
                error: function(err) {
                  console.log(err);
                }

            });

        }
</script>
@endsection

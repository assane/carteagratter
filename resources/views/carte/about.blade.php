@extends('layouts.master')

@section('title')
    About US
@endsection


@section('container')

    <hr class="invisible">

    <div class="row">

        <div class="col-md-6 col-md-offset-3">
            <div class="card card-inverse card-social text-xs-center">
                <div class="card-block has-gradient">
                    <img src="http://sen-challenge.com/img/logo.png" height="90" width="90" alt="Avatar" class="img-circle">
                    <h5 class="card-title">SEN-CHALLENGE</h5>
                        <a href="http://sen-challenge.com">  <h6 class="card-subtitle">http://sen-challenge.com/</h6> </a>
                    <a href="https://twitter.com/sen_challenge" class="btn btn-secondary-outline btn-sm">Follow</a>
                </div>
            </div>
        </div>

    </div>


@endsection



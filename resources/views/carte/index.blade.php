@extends('layouts.master')

@section('title')
Carte a gratter
@endsection

@section('container')

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
        <p align="center">  <h1>Ajouter un lot</h1> </p>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li><p style="color: red;font-weight: bolder">{{ $error }}</p></li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                   <strong style="color: green;font-weight: bolder">{{ Session::get('flash_message') }}</strong>
                </div>
            @endif

        <form id="formcreatelot" action="{{route('carte.ajouterlot')}}" enctype="multipart/form-data" method="post">
            <fieldset class="form-group">
                <input type="text" class="form-control" id="reference" name="reference" placeholder="Référence" >
            </fieldset>
            <fieldset class="form-group">
               <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" >
            </fieldset>
            <fieldset class="form-group">
                <input type="file" class="form-control" id="image" name="image" placeholder="Image lot" >
            </fieldset>
            <fieldset class="form-group">
                {!!   Form::select('utilisateur',[''=>'Utilisateurs']  + \App\Utilisateur::pluck('name','id')->toArray() ,null, array('class'=>'form-control','id'=>'utilisateur','') ) !!}
            </fieldset>
            <fieldset class="form-group">
                <select name="activer" id="activer" class="form-control">
                    <option  selected value="1">Activer</option>
                    <option value="0">Désactiver</option>
                </select>
            </fieldset>
            <button type="submit" class="btn col-md-12 btn-primary">Ajouter</button>
        </form>

         </div>   
    </div>    

@endsection
@extends('layouts.master')

@section('container')

    <h2> Dashboard :</h2>

    <div class="row">

        <div class="col-md-4">

            <div class="card card-inverse card-social text-xs-center">
                <div class="card-block has-gradient">
                    <img src="{{ URL::to('avatar/'.Auth::user()->avatar) }}" height="90" width="90" alt="Avatar" class="img-circle">
                    <h5 class="card-title"> {{Auth::user()->personne->prenom}} {{Auth::user()->personne->nom}} </h5>
                    <h6 class="card-subtitle">{{Auth::user()->personne->email}}</h6>
                </div>
            </div>
        </div>

        <div class="col-md-8">

            <div class="card card-chart">
                <ul class="list-group">
                    <li class="list-group-item complete">
                        <?php $datas = DB::table('utilisateurs')  ->join('personnes', 'utilisateurs.personne_id', '=', 'personnes.id')
                                ->join('pharmacies', 'personnes.pharmacie_id', '=', 'pharmacies.id')
                                ->select('*')
                                ->where('utilisateurs.id',Auth::user()->id)
                                ->get();
                            //dd($datas);
                        ?>

                        <?php  echo '<h5>'. $datas[0]->nom. '</h5>'; ?>
                        <input type="hidden" name="PharmacieCurrent" id="PharmacieCurrent" value="<?=$datas[0]->pharmacie_id?>"/>
                    </li>
                    <li class="list-group-item complete PhV">
                        <h2>Pharmacie en garde : </h2>
                        <div id="divetatPH" name="divetatPH" >
                            <?php
                            if($datas[0]->etatpharmacie_id == 1){
                                $checked = 'checked';
                            }
                            if($datas[0]->etatpharmacie_id == 2)
                                $checked = '';
                            ?>

                        <input  <?=$checked?>  data-toggle="toggle" data-on="Activer" data-off="Desactiver" data-onstyle="success" data-offstyle="danger" type="checkbox" id="etatPH" name="etatPH" />
                        </div>
                        <p align='center' class="loaderPH" id="loaderPH"><i class='fa fa-refresh fa-spin fa-5x fa-fw'></i><span class='sr-only'>Loading</span></p>
                    </li>
                </ul>

            </div>
    </div>

</div>
@endsection

@section('js')


<script>
    $("#loaderPH").hide();

    $(function() {
            $('#etatPH').change(function() {
            $('#divetatPH').hide();
            $("#loaderPH").show();
            var currentPharmacieID = $("#PharmacieCurrent").val();
            var etatfinal ;
            if($(this).prop('checked')){
                etatfinal = 1;
            }
            if(!$(this).prop('checked')){
                etatfinal = 2;
            }
            $.ajax({
                url: "{{route('pharmacie.updateEtatPH')}}",
                type: "POST",
                async : true,
                data  : {
                    'currentPharmacieID': currentPharmacieID,
                    'etatfinal': etatfinal
                },
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                beforeSend: function(){
                    $('#divetatPH').hide();
                    $("#loaderPH").show();
                },
                success:function(data){
                        if(data == 1){
                            alertify.alert('Succes', " Enregistrement effectué !!!", function(){ alertify.success('Ok'); });
                        }else {
                            alertify.alert('Error', "Erreur d'Enregistrement !!!", function(){ alertify.success('Ok'); });
                        }
                    $('#divetatPH').show();
                    $("#loaderPH").hide();
                },
                error:function () {
                    alertify.alert('Error', "Erreur d'Enregistrement !!!", function(){ alertify.success('Ok'); });
                    $('#divetatPH').show();
                    $("#loaderPH").hide();
                }
            });

        });
    });


</script>

@endsection
@extends('layouts.master')

@section('css')
    <link rel="stylesheet" href="{{ URL::to('css/b4table.css') }}" >
@endsection

@section('title')
    Carte a gratter
@endsection

@section('container')

    @if(Session::has('flash_message'))
        <p align="center">
        <div class="alert alert-success">
            <strong style="color: green;font-weight: bolder">{{ Session::get('flash_message') }}</strong>
        </div>
        </p>
    @endif

<br/>

    <div class="row">
        <div class="col-md-12">
            <table id="" class="table table-bordered table-hover" border="">
                <thead>
                <tr>
                    <th>Reference</th>
                    <th>Nombre</th>
                    <th>image</th>
                    <th>Statut</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody class="affichelot"></tbody>
            </table>
        </div>
    </div>

@endsection




@section('js')
    <script>
        show();
        function show (){
            $.ajax({
                url: "{{route('carte.show')}}",
                type: "POST",
                async : true,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data  : {
                    'showrecord':1
                },
                beforeSend: function(){
                    $(".affichelot").html("<i class='fa fa-refresh fa-spin fa-3x fa-fw'></i><span class='sr-only'>Loading</span>'");
                },
                success:function(s){
                    $('.affichelot').html(s);
                }
            });
        }
    </script>
@endsection
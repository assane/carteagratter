<?php 
namespace App\Http\Controllers;

use App\Lot;
use App\Utilisateur;
use Illuminate\Http\Request;
use App\Http\Requests\FormCreateUser;
use DB;
use File;
use Auth;
use Mail;
use Validator;
use Session; 
use Hash;

/**
 * Class CarteController
 * @package App\Http\Controllers
 */
class CarteController extends Controller {

/*
     public function __construct() {
        $this->middleware('auth', ['except' => ['login']]);
    }
*/

    public function hi() {
        return "hi it'a the api maxi quiz !!!";
    }

    public function index() {
      return view('carte.index');
    }


/*    public function showLotenable(Request $request){
        $datas = Lot::inRandomOrder()
                    ->where('activer',1)
                    ->first();
        return $datas;
    }*/

    public function showLotenable(){
        $datas = Lot::inRandomOrder()
                    ->where('activer',1)
                    ->first();
        return $datas;
    }

    public function UpdateLot(Request $request){
        $ref = $request->input('ref');
        $userid = $request->input('userid');
        $nombre = $request->input('nombre');
        if($nombre != 0){
                   try {
                       $bool = Lot::where('reference',$ref)
                        ->where('utilisateur_id',$userid)
                        ->decrement('nombre');
                            if($bool){
                            return response()->json([
                            "message" => "Mis à jour affectuée avec succés",
                            'statusCode' => '2000'
                            ]);
                            }
                     }catch(\Exception $e) {
                            return response()->json([
                            "message" => "Erreur survenue pendant la mise à jour",
                            'statusCode' => '5000'
                            ]);
                        } 
            }
    }

    public function create(Request $request) {
        $validator = Validator::make($request->all(), [
//            'reference' => 'required|unique:lots',
            'reference' => 'required',
            'nombre'    => 'required',
            'image' =>     'required|max:1000|mimes:jpeg,png,gif',
            'utilisateur'   => 'required',
            'activer'   => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->route('carte.index')
                ->withErrors($validator)
                ->withInput();
        }
        $lot = new Lot(array(
            'reference' => $request->get('reference'),
            'nombre'  => $request->get('nombre'),
            'image'   =>  $request->file('image')->getClientOriginalName(),
            'utilisateur_id'   =>  $request->get('utilisateur'),
            'activer' => $request->get('activer')
        ));
        if($lot->save()){
            $request->file('image')->move(base_path() . '/public/avatar/', $request->file('image')->getClientOriginalName());
            Session::flash('flash_message', 'Opération éffectuée avec succés !');
            return redirect()->back();
        }
    }
 
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function dellot (Request $request){
         $idlot = $request['idlot'];
         $lot = Lot::findOrFail($idlot);
         $lot->delete();
         Session::flash('flash_message', 'Opération éffectuée avec succés !');
         return redirect()->route('carte.show');
    }


    public function getshow() {
        if(Auth::check()){
	       return view('carte.liste');
	}else{
               return View('carte.login');
	}
    }

    public function show() {
        sleep(1);
        $show ="";
        $datas = Lot::all();
        $class="";
        foreach($datas as $data){
          $un_user = Utilisateur::where('id',$data->utilisateur_id)->first()->name;
            $etat = "";
            if($data->activer == 0){
                $etat='<i class=\'fa fa-thumbs-down fa-2x\' style=\'color:red\' aria-hidden=\'true\'></i>';
            }
            if($data->activer == 1){
                $etat='<i class=\'fa fa-thumbs-up fa-2x\' style=\'color:green\' aria-hidden=\'true\'></i>';
            }

            $show .= "<tr>
                    <td>$data->reference   &nbsp;&nbsp; <b>[$un_user]</b></td>
                    <td>$data->nombre</td>
                    <td><img src='/avatar/$data->image' height='50px' width='50px'/></td>
                    <td>$etat</td>
                    <td style='white-space:nowrap;'>
                        <form action='deletelot' method='post'>
                               <input type='hidden' name='idlot' value='$data->id'>
                              <button class='btn-sm btn-danger'>Suprrimer</button>  
                        </form>     
                             
                        <a class='btn-sm btn-primary' data-toggle='modal' href='#modal-$data->id'>Modifier</a>
                        
                        <div class='modal fade' id='modal-$data->id'>
                        <div class='modal-dialog'>
                        <div class='modal-content'>                                               
                             <div class='modal-body'>                       
                             <h1>Modifier le lot</h1>
                                <form action='editlot' name='editlot-$data->id' id='form-$data->id' method='post'>
                                        <input type='hidden' name='idlot' id='idlot' value='$data->id' />                               
                                        <fieldset class='form-group'>
                                          <input type='text' disabled class='form-control' id='name' name='name' value='$un_user' >
                                        </fieldset>
                                        <fieldset class='form-group'>
                                          <input type='text' disabled class='form-control' id='reference' name='reference' value='$data->reference' >
                                        </fieldset>
                                        <fieldset class='form-group'>
                                          <input type='text' class='form-control' id='nombre' name='nombre' value='$data->nombre' >
                                        </fieldset>	
                                        <fieldset class='form-group'>
                                            <select name='activer' id='activer' class='form-control'>
                                            <option  selected value='1'>Activer</option>
                                            <option value='0'>Désactiver</option>
                                            </select>
                                        </fieldset>
                                        <div class='modal-footer'>
                                        <button type='submit' class='btn-sm btn-primary'>Modifier</button>
                                        <button class='btn-sm btn-primary' data-dismiss='modal'>Annuler</button>
                                        </div>
                                    </form>
                            </div>
                        </div>
                        </div>
                        </div>
                    </td>
                    </tr>";
        }
        return $show;
    }


    /**
     * @return View Login
     */
    public function getlogin(){
        return View('carte.login');
    }


    /**
     * @param Request $request
     * @return int
     */
    public function connecter(Request $request){
        sleep(2);
        $user = $request->only('login','password');
        if(Auth::attempt($user)){
            return 1;
        }else{
            return 0;
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(){
        Auth::logout();
        Session::flush();
        return redirect()->route('carte.login');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function game(){
        if(Auth::check()){
   	    if(Auth::user()->name == "admin"){
	        return redirect()->route('carte.index');
	    }

	    $lot = Lot::inRandomOrder()
            ->where('utilisateur_id',Auth::user()->id)
            ->where('activer',1)
            ->first();
          return view('carte.game')->with('lot',$lot);  
        }else{
            return View('carte.login');
        }

    }

    public function getcreateuser(){
	if(Auth::check()){
               return view('carte.user');
        }else{
               return View('carte.login');
        }
    }
    public function getlistusers(){
        if(Auth::check()){
               return view('carte.listeusers');
        }else{
               return View('carte.login');
        }    
    }

    public function storenewuser(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:utilisateurs',
            'login'    => 'required|unique:utilisateurs|email',
            'password' => 'required|min:5|confirmed',
            'password_confirmation' => 'required|min:5',
        ]);
        
        if ($validator->fails()) {
            return redirect()->route('carte.getcreateuser')
                ->withErrors($validator)
                ->withInput();
        }
        
        $utilisateur = new Utilisateur(array(
            'name' => $request->get('name'),
            'login'  => $request->get('login'),
            'password'   =>  Hash::make($request->get('password')),
        ));
        
        if($utilisateur->save()){
            Session::flash('flash_message', 'Opération éffectuée avec succés !');
            return redirect()->back();
        }
    }

    public function listusers(){
        sleep(1);
        $show ="";
        $datas = Utilisateur::all();
        $class="";
        foreach($datas as $data){
            $etat = "";

            $show .= "<tr>
                    <td >$data->name </td>
                    <td >$data->login</td>
                    <td >
                        <form action='/deluser' method='post'>
                               <input type='hidden' name='iduser' value='$data->id'>
                              <button class='btn-sm btn-danger '>Suprrimer User ?</button>  
                        </form>                    
                    </td>
                    </tr>";
        }
        return $show;
    }

    public function deluser(Request $request){
        $iduser = $request['iduser'];
        $user = Utilisateur::findOrFail($iduser);
        $user->delete();
        Session::flash('flash_message', 'Opération éffectuée avec succés !');
        return redirect()->route('carte.getlistusers');
    }


    public function editlot(Request $request){
        $id = $request->get('idlot');

        $unlot = Lot::find($id);
        $unlot->nombre = $request->get('nombre');
        $unlot->activer = $request->get('activer');
        $unlot->save();

        Session::flash('flash_message', 'Opération éffectuée avec succés !');
        return redirect()->back();
    }

}

?>

<?php 

namespace App\Http\Controllers;

use App\Personne;
use App\Utilisateur;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Http\Requests\FormCreateUser;
use Tymon\JWTAuth\Facades\JWTAuth;
use DB;
use File;
use Auth;
use Mail;



/**
 * Class UtilisateurController
 * @package App\Http\Controllers
 */
class UtilisateurController extends Controller {

    /*
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }
    */

  public function index(){
  
  }

    /**
     * @param Request $request
     * @return int
     */
    public function connecter(Request $request){
        sleep(2);
        $user = $request->only('login','password');
        if(Auth::attempt($user)){
            return 1;
        }else{
            return 0;
        } 
    }


  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
    public  function profileUser (){
      try {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
          return response()->json(['user_not_found'], 404);
        }
      } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
        return response()->json(['token_expired'], $e->getStatusCode());
      } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
        return response()->json(['token_invalid'], $e->getStatusCode());
      } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
        return response()->json(['token_absent'], $e->getStatusCode());
     }
     return response()->json(compact('user'));
    }



  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
    public  function profilePersonne (){
      try {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
          return response()->json(['user_not_found'], 404);
        }
      } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
        return response()->json(['token_expired'], $e->getStatusCode());
      } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
        return response()->json(['token_invalid'], $e->getStatusCode());
      } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
        return response()->json(['token_absent'], $e->getStatusCode());
     }
      return $user->personne;    
    }

 /**
   * Display a listing of the resource.
   *
   * @return Response
   */
    public  function getProfile (){
      try {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
          return response()->json(['user_not_found'], 404);
        }
      } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
        return response()->json(['token_expired'], $e->getStatusCode());
      } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
        return response()->json(['token_invalid'], $e->getStatusCode());
      } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
        return response()->json(['token_absent'], $e->getStatusCode());
     }
      return $user->profile;    
    }




  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function showUser($id){
        return Utilisateur::find($id);
  }


}

?>
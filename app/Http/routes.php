<?php 
header('access-control-allow-origin: *');
header('Content-Type: application/json');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header('Access-Control-Allow-Credentials: true');
 
$api = app('Dingo\Api\Routing\Router');
$api->version('v1',function($api){
    $api->get('hi','App\Http\Controllers\CarteController@hi');
    $api->get('getlots','App\Http\Controllers\CarteController@showLotenable');
    $api->post('updatelot','App\Http\Controllers\CarteController@updateLot');
});

Route::get('/',function(){ return redirect()->route('carte.login');});
Route::get('/jeu', ['uses' => 'CarteController@game','as'  => 'carte.game']);
Route::get('/create', ['uses' => 'CarteController@index','as'  => 'carte.index']);
Route::get('/user', ['uses' => 'CarteController@getcreateuser','as'  => 'carte.getcreateuser']);
Route::get('/getlistusers', ['uses' => 'CarteController@getlistusers','as'  => 'carte.getlistusers']);
Route::post('/newuser', ['uses' => 'CarteController@storenewuser','as'  => 'carte.storenewuser']);
Route::post('/listusers', ['uses' => 'CarteController@listusers','as'  => 'carte.listusers']);
Route::post('/deluser', ['uses' => 'CarteController@deluser','as'  => 'carte.deluser']);
Route::get('/liste', ['uses' => 'CarteController@getshow','as'  => 'carte.getshow']);
Route::post('/liste', ['uses' => 'CarteController@show','as'  => 'carte.show']);
Route::post('/createlot', ['uses' => 'CarteController@create','as'  => 'carte.ajouterlot']);
Route::post('/deletelot', ['uses' => 'CarteController@dellot','as'  => 'carte.destroy']);
Route::post('/editlot', ['uses' => 'CarteController@editlot','as'  => 'carte.editlot']);
Route::get('/login', ['uses' => 'CarteController@getlogin', 'as'  => 'carte.login']);
Route::post('/connecter', ['uses' => 'CarteController@connecter', 'as'  => 'carte.connecter']);
Route::get('/logout', ['uses' => 'CarteController@logout', 'as'  => 'carte.logout']);
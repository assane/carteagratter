<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Pharmacie
 * @package App
 */
class Lot extends Model {
 
    protected $table = 'lots';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reference', 'nombre','image','activer','utilisateur_id'
    ];

    public function getuser(){
        //return "AAAAA";
        return $this->belongsTo('\App\Utilisateur');
    }

}